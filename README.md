# Documentation
- Create .env file in root directory for runing laravel.
- Copy all from .env.example and add to .env.
- Generate app key for laravel.
    ```
       php artisan key:generate
    ```
- Create .env file and copy content from .env.example and paste in .env.
- Add db configs in .env.
- Create tables in db.
    ```
       php artisan migrate
    ```
- Install composer.
    ```
        composer install
    ```   
- Install npm.
    ```
        npm install
    ```
- After installation npm run webpack.
    ```
        npm run build
    ```
- Add user by type "agent".
    ```
       php artisan db:seed
    ```
    email: agent@agent.com\
    password: 12345678
    
- Run php server for using
    ```
        php artisan serve
    ```
- After php server run open this link for using - **[127.0.0.1:8000](http://127.0.0.1:8000/)**
