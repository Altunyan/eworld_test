/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',

    data: {
        messages: [],
        newMessage: '',
        currentUser: null,
        sendTo: null,
        allUsers: [],
        chats: [],
    },

    created() {
        this.fetchMessages();

        Echo.private('chat')
            .listen('MessageSentEvent', (e) => {
                this.addMessageToArray(e.message, e.user);
            });
    },

    updated() {
        let container = '';

        if (this.sendTo) {
            container = this.$el.querySelector('.chat[data-id="' + this.sendTo + '"] .chat_body');
        } else {
            container = this.$el.querySelector('.messages');
        }

        container.scrollTop = container.scrollHeight;
    },

    methods: {
        fetchMessages() {
            axios.get('/messages').then(response => {
                this.messages = response.data.messages;
                this.currentUser = response.data.user;

                if (this.currentUser.type === 'agent') {
                    this.fetchAllUsers();
                }
            });
        },

        fetchAllUsers() {
            axios.get('/users').then(response => {
                this.allUsers = response.data;
            });
        },

        addMessage(message, sendTo) {
            axios.post('/messages', {
                message,
                sendTo
            }).then(response => {
                this.addMessageToArray(response.data.message, response.data.user);
            });
        },

        addMessageToArray(message, user) {
            if (this.currentUser.type === 'agent') {
                this.chats.forEach((chat) => {
                    if (chat.user.id === message.user_id || chat.user.id === message.send_to_user_id) {
                        chat.messages.push(message);
                        return false;
                    }
                });


            } else {
                this.messages.push({
                    message: message.message,
                    user: user
                });
            }
        },

        getUserMessagesById(id, agentId) {
            axios.get('/getUserMessagesById/' + id + '/' + agentId).then(response => {
                let user = null,
                    updated = false;

                this.allUsers.forEach((userData) => {
                    if (userData.id === id) {
                        user = userData;
                        return false;
                    }
                });

                this.chats.forEach((chat) => {
                    if (chat.user.id === id) {
                        chat.user = user;
                        chat.messages = response.data;
                        updated = true;
                    }
                });

                if (!updated) {
                    if (this.chats.length === 4) {
                        this.chats.shift();
                    }

                    this.chats.push({
                        user: user,
                        messages: response.data
                    });
                }
            });
        },

        addChat(id) {
            this.getUserMessagesById(id, this.currentUser.id);
            this.sendTo = id;
        },

        sendMessage() {
            let selectedChatIndex = null;

            if (this.sendTo) {
                this.chats.forEach((chat, index) => {
                    console.log(chat.user.id, this.sendTo)
                    if (chat.user.id === this.sendTo) {
                        this.newMessage = chat.newMessage;
                        selectedChatIndex = index;
                    }
                });
            }

            if (this.newMessage) {
                this.addMessage(this.newMessage, this.sendTo);
                this.newMessage = '';
                this.chats[selectedChatIndex].newMessage = '';
            }
        }
    }
});
