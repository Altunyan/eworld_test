<div
        class="clearfix"
        v-for="message in messages"
>
    <div class="is_my_message" v-if="message.user.id == currentUser.id">@{{ message.message }}</div>
    <div class="other_message" v-else>@{{ message.message }}</div>
</div>
