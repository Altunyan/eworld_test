@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if(Auth::user()->type == 'customer')
                <div class="col-md-8">
                    <h2>Messages</h2>
                    <div class="messages" data-module="messages">
                        @include('chat/messages')
                    </div>
                    <div class="input-group">
                        <input
                                type="text"
                                name="message"
                                class="form-control"
                                placeholder="Type your message here..."
                                v-model="newMessage"
                                @keyup.enter="sendMessage"
                        >
                        <button
                                class="btn btn-primary"
                                @click="sendMessage"
                        >
                            Send
                        </button>
                    </div>
                </div>
            @else
                @include('chat/users')
            @endif
        </div>
        @if (Auth::user()->type == 'agent')
            @include('chat/chats')
        @endif
    </div>
@endsection
