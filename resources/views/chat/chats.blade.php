<div class="chats" data-module="chats" v-if="chats.length">
    <div
            v-for="(chat, index) in chats"
    >
        <div class="chat" :data-id="chat.user.id">
            <h4 class="chat_header">Name: @{{ chat.user.name }}</h4>
            <div class="chat_body">
                <div
                        class="clearfix messages"
                        v-for="message in chat.messages"
                >
                    <div class="is_my_message" v-if="message.user_id == currentUser.id">@{{ message.message }}</div>
                    <div class="other_message" v-else>@{{ message.message }}</div>
                </div>
            </div>
            <div class="chat_footer">
                <div class="input-group">
                    <input
                            type="text"
                            name="message"
                            class="form-control"
                            placeholder="Type your message here..."
                            v-model="chat.newMessage"
                            @keyup.enter="sendMessage"
                            @click="addChat(chat.user.id)"
                    >
                    <button
                            class="btn btn-primary"
                            @click="sendMessage"
                    >
                        Send
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
