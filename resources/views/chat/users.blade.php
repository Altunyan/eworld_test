<div data-module="users">
    <div
            class="clearfix user"
            v-for="user in allUsers"
    >
        <div
                class="border text-black-50 p-1 m-1 font-weight-bold"
                @click="addChat(user.id)"
        >
            @{{ user.name }}
        </div>
    </div>
</div>

