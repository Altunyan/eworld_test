<?php

namespace Database\Seeders;

use App\Models\MySQL\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class AgentSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$agents = [
			[
				'name' => 'Agent',
				'email' => 'agent@agent.com',
				'password' => Hash::make('12345678'),
			]
		];

		Schema::disableForeignKeyConstraints();
		foreach ($agents as $agent) {
			if (!User::where('email', $agent['email'])->first())
				User::create($agent);
		}
		Schema::enableForeignKeyConstraints();
	}
}
