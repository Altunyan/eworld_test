<?php

namespace App\Http\Controllers;

use App\Events\MessageSentEvent;
use App\Models\MySQL\Message;
use App\Models\MySQL\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
	/**
	 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public function index()
	{
		$user = Auth::user();

		$messages = Message::where('user_id', $user->id)
			->orWhere('send_to_user_id', $user->id)
			->with('user')
			->get();

		return [
			'messages' => $messages,
			'user' => $user,
		];
	}

	/**
	 * @param Request $request
	 * @return array
	 */
	public function store(Request $request)
	{
		$user = Auth::user();

		if (!$request->input('sendTo')) {
			$request->sendTo = User::where('type', 'agent')->first()->id ?? null;
		}

		$message = $user->messages()->create([
			'message' => $request->input('message'),
			'send_to_user_id' => $request->sendTo,
		]);

		broadcast(new MessageSentEvent($message, $user))->toOthers();

		return [
			'message' => $message,
			'user' => $user,
		];
	}

	/**
	 * get all users
	 *
	 * @return mixed
	 */
	public function allUsers()
	{
		return User::where('type', '!=', 'agent')->get();
	}

	/**
	 * get user massages by id
	 *
	 * @param $id
	 * @param $agentId
	 * @return mixed
	 */
	public function getUserMessagesById($id, $agentId)
	{
		$messages = Message::where(function($query) use ($id, $agentId) {
				return $query->where('user_id', $id)
					->where('send_to_user_id', $agentId);
			})
			->orWhere(function($query) use ($id, $agentId) {
				return $query->where('user_id', $agentId)
					->where('send_to_user_id', $id);
			})
			->get();

		return $messages;
	}
}
